from socket import *
import select
from random import randint
import pygame
from math import floor
import json
import threading
import sys
from game_client import *

HOST = "127.0.0.1"
PORT = 5555
SIZE = 8
CHAR = "Arthan"

client = socket(AF_INET, SOCK_STREAM)
client.connect((HOST, PORT))
client.setblocking(0)

pygame.init()
window = pygame.display.set_mode((0, 0), pygame.FULLSCREEN)

game = Game(client, window, SIZE)

class Ground:
    def __init__(self):
        self.image = pygame.image.load("Assets/map.png")
        self.x = 0
        self.y = 0

    def draw(self):
        window.blit(self.image, (self.x, self.y))

def login(data):
    game.char = Character(game, data['name'], data['x'], data['y'])
    game.char.stones = data['stones']
    game.char.woods = data['woods']
    game.characters.append(game.char)
    
def add_char(data):
    game.characters.append(Character(game, data['name'], data['x'], data['y']))

def del_char(data):
    char = game.get_char_by_name(data['name'])
    if char:
        game.characters.remove(char)

def update_char(data):
    char = game.get_char_by_name(data['name'])
    if char:
        char.woods = data['woods']
        char.stones = data['stones']
        
def char_pos(data):
    char = game.get_char_by_name(data['name'])
    char.x = data['x']
    char.y = data['y']

def add_trees(data):
    for pos in data:
        game.trees.append(Tree(game, pos['x'], pos['y'], pos['id']))

def add_stones(data):
    for pos in data:
        game.stones.append(Stone(game, pos['x'], pos['y'], pos['id']))

def add_sheeps(data):
    for pos in data:
        game.sheeps.append(Sheep(game, pos['x'], pos['y']))
    
def del_tree(tree_id):
    tree = game.get_tree_by_id(tree_id)
    if tree:
        game.trees.remove(tree)

def del_stone(stone_id):
    stone = game.get_stone_by_id(stone_id)
    if stone:
        game.stones.remove(stone)

def communication():
    while game.run:
        ready = select.select([client], [], [], 2)
        if ready[0]:
            txt_size = int(client.recv(SIZE).decode("utf8"))
            txt = client.recv(txt_size).decode("utf8")
            #print("recv: '" + txt + "'")
            msgs = txt.split(';')
            for msg in msgs:
                if len(msg.strip()) == 0:
                  continue
                #print(msg)
                data = json.loads(msg)
                if data['cmd'] == 'who_are_you':
                    game.send('LOGIN', CHAR)
                elif data['cmd'] == 'BAD_LOGIN':
                    print('bad login!')
                    game.run = False
                elif data['cmd'] == 'LOGIN':
                    login(data['params'])
                elif data['cmd'] == 'ADD_CHAR':
                    add_char(data['params'])
                elif data['cmd'] == 'DEL_CHAR':
                    del_char(data['params'])                    
                elif data['cmd'] == 'CHAR_POS':
                    char_pos(data['params'])
                elif data['cmd'] == 'ADD_TREES':
                    add_trees(data['params'])
                elif data['cmd'] == 'ADD_STONES':
                    add_stones(data['params'])
                elif data['cmd'] == 'ADD_SHEEPS':
                    add_sheeps(data['params'])
                elif data['cmd'] == 'DEL_TREE':
                    del_tree(data['params'])
                elif data['cmd'] == 'DEL_STONE':
                    del_stone(data['params'])
                elif data['cmd'] == 'UPDATE_CHAR':
                    update_char(data['params'])

def main():
    game.char_name = CHAR
    ground = Ground()

    thread = threading.Thread(target=communication)
    thread.start()

    while game.run:
        keys = pygame.key.get_pressed()
        events = pygame.event.get()
        for event in events:
            if event.type == pygame.QUIT:
                game.run = False

        if keys[pygame.K_c]:
            game.run = False

        game.tick(keys)
        ground.draw()
        game.draw()
        
        pygame.display.update()
    
    pygame.quit()
    client.shutdown(SHUT_RDWR)
        
if __name__ == "__main__":
    main()