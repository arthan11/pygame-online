from random import randint
import json

class Character():
    def __init__(self, game, name, x, y):
        self.game = game
        self.name = name
        self.x = x
        self.y = y
        self.speed = 8
        self.stones = 0
        self.woods = 0
        self.logged_in = False
        self.conn = None
        
    def __str__(self):
        return f'{self.name}: {self.x}x{self.y}'

    def public_info(self):
        return {'name': self.name, 'x': self.x, 'y': self.y}

    def private_info(self):
        return {'name': self.name, 'x': self.x, 'y': self.y, 'stones': self.stones, 'woods': self.woods}

    def move(self, direction):
        if direction == 'DOWN':
            self.y += self.speed
        elif direction == 'UP':
            self.y -= self.speed
        elif direction == 'LEFT':
            self.x -= self.speed
        elif direction == 'RIGHT':
            self.x += self.speed

class Tree():
    next_id = 1
    def __init__(self, game, x, y, id=None):
        if id:
            self.id = id
        else:
            self.id = Tree.next_id
            Tree.next_id += 1
        self.game = game
        self.x = x
        self.y = y

class Sheep():
    def __init__(self, game, x, y):
        self.game = game
        self.x = x
        self.y = y

class Stone():
    next_id = 1
    def __init__(self, game, x, y, id=None):
        if id:
            self.id = id
        else:
            self.id = Stone.next_id
            Stone.next_id += 1
        self.game = game
        self.x = x
        self.y = y

class Game():
    def __init__(self):
        self.char_name = None
        self.char = None
        self.characters = []
        self.trees_max = 30
        self.stones_max = 50
        self.trees = []
        self.stones = []
        self.sheeps = []
        self.map_with = 1300
        self.map_height = 800
        
    def addTrees(self, amount):
        for i in range(amount):
            x = randint(0, self.map_with-1)
            y = randint(0, self.map_height-1)
            tree = Tree(self, x, y)
            self.trees.append(tree)
      
    def addStones(self, amount):
        for i in range(amount):
            x = randint(0, self.map_with-1)
            y = randint(0, self.map_height-1)
            stone = Stone(self, x, y)
            self.stones.append(stone)
            
    def addSheeps(self, amount):
        for i in range(amount):
            x = randint(0, self.map_with-1)
            y = randint(0, self.map_height-1)
            sheep = Sheep(self, x, y)
            self.sheeps.append(sheep)
            
    def get_char_by_conn(self, conn):
        for char in self.characters:
            if char.conn == conn:
                return char
        return None
        
    def get_char_by_name(self, name):
        for char in self.characters:
            if char.name == name:
                return char
        return None

    def get_tree_by_id(self, id):
        for tree in self.trees:
            if tree.id == id:
                return tree
        return None
        
    def get_stone_by_id(self, id):
        for stone in self.stones:
            if stone.id == id:
                return stone
        return None
    
if __name__ == '__main__':
    game = Game()
    
    game.characters.append(Character(game, 'Arthan', 860, 450))
    game.characters.append(Character(game, 'Player2', 900, 450))
    game.addTrees(100)
    game.addStones(100)
    game.addSheeps(100)
    print(len(game.trees))
    print(len(game.stones))
    print(len(game.sheeps))
    print(game.trees[0].x, game.trees[0].y)
    print(game.characters[0])
    print(game.characters[1])
    