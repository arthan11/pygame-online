import game
import pygame
import json

class Character(game.Character):
    def __init__(self, game, name, x, y):
        super().__init__(game, name, x, y)
        self.stand_img = pygame.image.load("Assets/postać.png")
        self.width = self.stand_img.get_width()
        self.height = self.stand_img.get_height()
        self.hitbox = pygame.Rect(self.x, self.y, self.width, self.height)

    def tick(self, keys):
        self.hitbox = pygame.Rect(self.x, self.y, self.width, self.height)
        
        if self == self.game.char:
            if keys[pygame.K_w]:
                self.game.send('MOVE', 'UP')
                self.move('UP')
            if keys[pygame.K_a]:
                self.game.send('MOVE', 'LEFT')
                self.move('LEFT')
            if keys[pygame.K_s]:
                self.game.send('MOVE', 'DOWN')
                self.move('DOWN')
            if keys[pygame.K_d]:
                self.game.send('MOVE', 'RIGHT')
                self.move('RIGHT')

    def draw(self):
        self.game.window.blit(self.stand_img, (self.x, self.y))
        
        img_name = self.game.font.render(self.name, True, (255,255,0))
        self.game.window.blit(img_name, (self.x, self.y-20))

class Tree(game.Tree):
    def __init__(self, game, x, y, id):
        super().__init__(game, x, y, id)
        self.image = pygame.image.load("Assets/gracz.png")
        self.width = self.image.get_width()
        self.height = self.image.get_height()
        self.hitbox = pygame.Rect(self.x, self.y, self.width, self.height)
           
    def draw(self):
        self.game.window.blit(self.image, (self.x, self.y))

class Stone(game.Stone):
    def __init__(self, game, x, y, id):
        super().__init__(game, x, y, id)
        self.image = pygame.image.load("Assets/1 (3).png")
        self.width = self.image.get_width()
        self.height = self.image.get_height()
        self.hitbox = pygame.Rect(self.x, self.y, self.width, self.height)

    def draw(self):
        self.game.window.blit(self.image, (self.x, self.y))

class Sheep(game.Sheep):
    def __init__(self, game, x, y):
        super().__init__(game, x, y)
        self.image = pygame.image.load("Assets/zwierze.png")
        self.width = self.image.get_width()
        self.height = self.image.get_height()
        self.hitbox = pygame.Rect(self.x, self.y, self.width, self.height)
           
    def draw(self):
        self.game.window.blit(self.image, (self.x, self.y))
           
class Game(game.Game):
    def __init__(self, client, window, size):
        super().__init__()
        self.client = client
        self.window = window
        self.HEADER_SIZE = size
        self.run = True

        sysfont = pygame.font.get_default_font()
        self.font = pygame.font.SysFont(sysfont, 24)
        

    def send(self, cmd, params=None):
        if params != None:
            msg = json.dumps({'cmd': cmd, 'params': params})
        else:
            msg = json.dumps({'cmd': cmd})
        msg = msg.encode("utf8")
        #msg += ';'
        msg_size = f"{len(msg):<{self.HEADER_SIZE}}".encode("utf8")
        #print(msg_size + msg)
        self.client.send(msg_size + msg)

    def draw_stats(self):
        if self.char:
            img_name = self.font.render(self.char.name, True, (255,255,0))
            self.window.blit(img_name, (10, 10))
            img_woods = self.font.render(f'Woods: {self.char.woods}', True, (255,255,0))
            self.window.blit(img_woods, (10, 30))
            img_stones = self.font.render(f'Stones: {self.char.stones}', True, (255,255,0))
            self.window.blit(img_stones, (10, 50))

    def draw(self):
        for tree in self.trees:
            tree.draw()
        for stone in self.stones:
            stone.draw()
        for sheep in self.sheeps:
            sheep.draw()

        for character in self.characters:
            character.draw()
            
        self.draw_stats()
            
    def tick(self, keys):
        for character in self.characters:
            character.tick(keys)

        if pygame.mouse.get_pressed()[0]:            
            for tree in self.trees:
                if self.char.hitbox.colliderect(tree.hitbox):
                    self.send('CUT_TREE', tree.id)
            for stone in self.stones:
                if self.char.hitbox.colliderect(stone.hitbox):
                    self.send('CUT_STONE', stone.id)

