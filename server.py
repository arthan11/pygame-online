from socket import *
import threading
import select
from game import Game, Character
import json

HOST = ""
PORT = 5555
SIZE = 8

game = Game()
game.addTrees(game.trees_max)
game.addStones(game.stones_max)
game.addSheeps(50)
game.characters.append(Character(game, 'Arthan', 860, 450))
game.characters.append(Character(game, 'Player2', 900, 450))
#game.characters[0].stones = 10
#game.characters[0].woods = 15

server = socket(AF_INET, SOCK_STREAM)
server.bind((HOST, PORT))
server.listen(2)

connections = []

def send(conn, cmd, params=None):
    if params != None:
        msg = json.dumps({'cmd': cmd, 'params': params})
    else:
        msg = json.dumps({'cmd': cmd})
    msg = msg.encode("utf8")
    #msg += ';'
    msg_size = f"{len(msg):<{SIZE}}".encode("utf8")
    #print(msg_size + msg)
    conn.send(msg_size + msg)

def send_others(main_char, cmd, params=None):
    for char in game.characters:
        if char.logged_in and (char != main_char):
            send(char.conn, cmd, params)
    
def send_to_all(cmd, params=None):
    for char in game.characters:
        if char.logged_in:# and (char != main_char):
            send(char.conn, cmd, params)    
    
def login(char_name, conn):
    for char in game.characters:
        if char.name == char_name and not char.logged_in:
            char.logged_in = True
            char.conn = conn
            send(conn, 'LOGIN', char.private_info())
            send_others(char, 'ADD_CHAR', char.public_info())
            #send_to_all('ADD_CHAR', char.public_info())
            return True
    send(conn, 'BAD_LOGIN')

def logout(conn):
    char = game.get_char_by_conn(conn)
    if char:
        char.logged_in = False
        char.conn = None
        send_to_all("DEL_CHAR", {"name": char.name})

def cut_tree(tree_id, conn):
    char = game.get_char_by_conn(conn)
    tree = game.get_tree_by_id(tree_id)
    if char and tree:
        game.trees.remove(tree)
        send_to_all("DEL_TREE", tree_id)
        char.woods += 1
        send(conn, "UPDATE_CHAR", char.private_info())

def cut_stone(stone_id, conn):
    char = game.get_char_by_conn(conn)
    stone = game.get_stone_by_id(stone_id)
    if char and stone:
        game.stones.remove(stone)
        send_to_all("DEL_STONE", stone_id)
        char.stones += 1
        send(conn, "UPDATE_CHAR", char.private_info())
    
def move_char(direction, conn):
    #print(direction)
    char = game.get_char_by_conn(conn)
    if char:
        char.move(direction)
        send_to_all("CHAR_POS", {"name": char.name, "x": char.x, "y": char.y})

def main(conn, addres):
    try:
    #if True:
        print(f"Uzyskano połączenie od {addres}")
        connections.append(conn)

        send(conn, "who_are_you")

        while True:
            ready = select.select([conn], [], [], 2)
            if ready[0]:
                txt_size = int(conn.recv(SIZE).decode("utf8"))
                txt = conn.recv(txt_size).decode("utf8")
                msgs = txt.split(';')
                for msg in msgs:
                    if len(msg.strip()) == 0:
                      continue
                    data = json.loads(msg)
                    if data['cmd'] == 'LOGIN':
                        if login(data['params'], conn):
                            for char in game.characters:
                                if char.name != data['params'] and char.logged_in:
                                    send(conn, 'ADD_CHAR', char.public_info())
                            
                            trees = []
                            for tree in game.trees:
                                trees.append({'id': tree.id, 'x': tree.x, 'y': tree.y})
                                if len(trees) >= 20:
                                    send(conn, 'ADD_TREES', trees)
                                    trees = []
                            if len(trees) > 0:
                                send(conn, 'ADD_TREES', trees)
                                
                            
                            stones = []
                            for stone in game.stones:
                                stones.append({'id': stone.id, 'x': stone.x, 'y': stone.y})
                                if len(stones) >= 20:
                                    send(conn, 'ADD_STONES', stones)
                                    stones = []
                            if len(stones) > 0:
                                send(conn, 'ADD_STONES', stones)
                                    
                            
                            sheeps = []
                            for sheep in game.sheeps:
                                sheeps.append({'x': sheep.x, 'y': sheep.y})
                                if len(sheeps) >= 20:
                                    send(conn, 'ADD_SHEEPS', sheeps)
                                    sheeps = []
                            if len(sheeps) > 0:
                                send(conn, 'ADD_SHEEPS', sheeps)
                                    
                    elif data['cmd'] == 'MOVE':
                        move_char(data['params'], conn)
                    elif data['cmd'] == 'CUT_TREE':
                        cut_tree(data['params'], conn)
                    elif data['cmd'] == 'CUT_STONE':
                        cut_stone(data['params'], conn)
                
                if len(game.trees) < game.trees_max:
                    game.addTrees(1)
                    tree = game.trees[-1]
                    send_to_all('ADD_TREES', [{'id': tree.id, 'x': tree.x, 'y': tree.y}])

                if len(game.stones) < game.stones_max:
                    game.addStones(1)
                    stone = game.stones[-1]
                    send_to_all('ADD_STONES', [{'id': stone.id, 'x': stone.x, 'y': stone.y}])
                
    except:
        print(f"Disconnect {addres}")
        logout(conn)
        connections.remove(conn)
        
while True:
    conn, addres = server.accept()

    thread = threading.Thread(target=main, args=(conn, addres))
    thread.start()
